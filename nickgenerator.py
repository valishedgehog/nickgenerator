#!/usr/bin/python3

import os

import click

from src.datasets import Dataset
from src.generator import generate_nickname, NickCase

FILE_PATH = os.path.dirname(os.path.realpath(__file__))
DEFAULT_PATH = FILE_PATH + '/data'


@click.command()
@click.option('--count', '-n', default=1, help='Count of nicknames')
@click.option('--case', '-c',
              type=click.Choice(NickCase.__members__),
              default='SNAKE_CASE',
              callback=lambda c, p, v: getattr(NickCase, v) if v else None,
              help='Case of nickname')
@click.option('--dataset', '-d',
              type=click.Choice(Dataset.__members__),
              default='ANIMALS',
              callback=lambda c, p, v: getattr(Dataset, v) if v else None,
              help='Data set for nick generation')
def nick_generator(count, case, dataset):
    nicknames = generate_nickname(FILE_PATH, count, case, dataset)
    for nickname in nicknames:
        print(nickname)


if __name__ == '__main__':
    nick_generator()

# Nick generator

Simple Python 3 nickname generator

Usage:
```bash
pip install -r requirements.txt
source ./venv/bin/activate.sh
./nickgenerator.py --help
```

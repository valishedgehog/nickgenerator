import random
import re
from os import listdir
from os.path import isfile, join

from src.cases import NickCase, generate_with_case
from src.datasets import Dataset


def generate_nickname(base_path: str, count: int, case: NickCase, dataset: Dataset) -> list:
    path = base_path + '/data' + '/' + dataset.value
    files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]

    data = []
    for file in files:
        with open(file, 'r') as f:
            data.append(f.readlines())

    result = []
    for i in range(0, count):
        nick = []
        for file_data in data:
            idx = random.randint(0, len(file_data) - 1)
            nick.extend([s.strip().lower() for s in re.split(r"\s|-|_", file_data[idx])[:-1]])
        result.append(generate_with_case(nick, case))

    return result

from enum import Enum


class NickCase(Enum):
    FLAT_CASE = 1
    KEBAB_CASE = 2
    CAMEL_CASE = 3
    PASCAL_CASE = 4
    SNAKE_CASE = 5


def generate_with_case(nick: list, case: NickCase) -> str:
    if case == NickCase.FLAT_CASE:
        return ''.join(nick)
    if case == NickCase.KEBAB_CASE:
        return '-'.join(nick)
    if case == NickCase.CAMEL_CASE:
        return nick[0] + ''.join([s.capitalize() for s in nick[1:]])
    if case == NickCase.PASCAL_CASE:
        return ''.join([s.capitalize() for s in nick])
    if case == NickCase.SNAKE_CASE:
        return '_'.join(nick)
